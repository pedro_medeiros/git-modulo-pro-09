package br.com.mentorama.demo.repository;


import br.com.mentorama.demo.Demo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Async("customThreadPool")
public interface DemoRepository extends JpaRepository<Demo, Long> {

    CompletableFuture<Optional<Demo>> findOneById(final Long id);

    CompletableFuture<List<Demo>> findAllBy();

}
