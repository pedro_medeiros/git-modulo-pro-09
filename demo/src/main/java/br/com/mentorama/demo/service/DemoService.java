package br.com.mentorama.demo.service;


import br.com.mentorama.demo.Demo;
import br.com.mentorama.demo.repository.DemoRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class DemoService {

    private final DemoRepository demoRepository;

    public DemoService(DemoRepository demoRepository){
        this.demoRepository = demoRepository;
    }

    public CompletableFuture<Optional<Demo>> findById(Long id){
    System.out.println("ServiceThread : " + Thread.currentThread().getName());
    return this.demoRepository.findOneById(id);
    }

    public CompletableFuture<List<Demo>> findAll(){
        System.out.println("ServiceThread : " + Thread.currentThread().getName());
        return demoRepository.findAllBy();
    }

    public CompletableFuture<Demo> save(Demo demo){
        System.out.println("ServiceTread : " + Thread.currentThread().getName());
        return CompletableFuture.completedFuture(demoRepository.save(demo));
    }
}
