package br.com.mentorama.demo.controller;


import br.com.mentorama.demo.Demo;
import br.com.mentorama.demo.exceptions.DemoNotFound;
import br.com.mentorama.demo.service.DemoService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/demo")
@Async
public class DemoController {

    public final DemoService demoservice;

    public DemoController(DemoService demoservice){
        this.demoservice = demoservice;
    }

    @GetMapping
    public CompletableFuture<List<Demo>> findAll(){
        System.out.println("Contoller Thread : " + Thread.currentThread().getName());
        return this.demoservice.findAll();
    }
    @GetMapping("/{id}")
    public CompletableFuture<Demo> findById(@PathVariable("id") final Long id){
        System.out.println("Controller Thread : " + Thread.currentThread().getName());
        return this.demoservice.findById(id)
        .thenApply(x -> x.orElseThrow(DemoNotFound::new));
    }

    @PostMapping
    public CompletableFuture<Demo> save(@RequestBody final Demo demo){
        System.out.println("Controller Thread : " + Thread.currentThread().getName());
        return this.demoservice.save(demo);
    }
}
